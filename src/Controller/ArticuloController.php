<?php

namespace App\Controller;

use App\Entity\Articulo;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticuloController extends AbstractController
{
    /**
     * @Route("/articulo", name="app_articulo")
     */
    public function index(ManagerRegistry $doctrine): Response
    {   

        $articulo = new Articulo();

        $articulo->setTitulo("Audifonos");

        $em = $doctrine->getManager();
        /*$em->persist($articulo);
        $em->flush();
        return new Response("Articulo insertado");*/

        $getArticulo = $em->getRepository(Articulo::class)->findOneBy([
            'id' => 2
        ]);
        /*
        return $this->render('articulo/index.html.twig', [
            'articulo' => $getArticulo,
        ]);*/

        $em->remove($getArticulo);
        $em->flush();
        return new Response("Articulo eliminado");
    }
}
